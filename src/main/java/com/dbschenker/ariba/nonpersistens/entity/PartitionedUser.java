package com.dbschenker.ariba.nonpersistens.entity;

import com.univocity.parsers.annotations.Parsed;

import lombok.Getter;
import lombok.Setter;

/**
 * entity class, represent user downloaded from the Downstream
 * 
 * @author juraj
 *
 */
public class PartitionedUser {

    @Getter
    @Setter
    @Parsed(field = "UniqueName")
    private String uniqueName;
    @Getter
    @Setter
    @Parsed(field = "PasswordAdapter")
    private String passwordAdapter;

}
