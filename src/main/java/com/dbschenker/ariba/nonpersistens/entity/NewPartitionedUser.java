package com.dbschenker.ariba.nonpersistens.entity;

import com.dbschenker.ariba.constants.Constants;
import com.univocity.parsers.annotations.Parsed;

import lombok.Getter;
import lombok.Setter;

/**
 * entity class, represents user to be uploaded into the Downstream
 * 
 * @author juraj
 *
 */
public class NewPartitionedUser {

    @Getter
    @Setter
    @Parsed(field = "UniqueName")
    private String uniqueName;
    @Getter
    @Setter
    @Parsed(field = "PasswordAdapter")
    private String passwordAdapter;
    @Getter
    @Setter
    @Parsed(field = "SAPPurchaseOrg")
    private String purchaseOrg;
    @Getter
    @Setter
    @Parsed(field = "Name")
    private String name;
    @Getter
    @Setter
    @Parsed(field = "EmailAddress")
    private String emailAddress;
    @Getter
    @Setter
    @Parsed(field = "DefaultCurrency.UniqueName")
    private String defaultCurrency;
    @Getter
    @Setter
    @Parsed(field = "TimeZoneID")
    private String timeZoneId;
    @Getter
    @Setter
    @Parsed(field = "LocaleID.UniqueName")
    private String preferredLocale;

    public NewPartitionedUser(CommonUser commonUser) {
        this.uniqueName = commonUser.getLoginId();
        this.passwordAdapter = Constants.PASSWORDADAPTER_1;
        this.purchaseOrg = Constants.PURCHASE_ORGANIZATION;
        this.name = commonUser.getFullName();
        this.emailAddress = commonUser.getEmailAddress();
        this.defaultCurrency = commonUser.getDefaultCurrency();
//        this.timeZoneId = commonUser.getTimeZoneId();
        this.preferredLocale = commonUser.getPreferredLocale();
    }

}
