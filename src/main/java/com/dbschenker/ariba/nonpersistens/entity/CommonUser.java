package com.dbschenker.ariba.nonpersistens.entity;

import com.univocity.parsers.annotations.Parsed;

import lombok.Getter;
import lombok.Setter;

/**
 * entity class, represents Common user downloaded from the Upstream
 * 
 * @author juraj
 *
 */
public class CommonUser {

    @Getter
    @Setter
    @Parsed(field = "UniqueName")
    private String loginId;
    @Getter
    @Setter
    @Parsed(field = "PasswordAdapter")
    private String passwordAdapter;
    @Getter
    @Setter
    @Parsed(field = "Name")
    private String fullName;
    @Getter
    @Setter
    @Parsed(field = "EmailAddress")
    private String emailAddress;
    @Getter
    @Setter
    @Parsed(field = "DefaultCurrency.UniqueName")
    private String defaultCurrency;
    @Getter
    @Setter
    @Parsed(field = "TimeZoneID")
    private String timeZoneId;
    @Getter
    @Setter
    @Parsed(field = "LocaleID.UniqueName")
    private String preferredLocale;

}
