package com.dbschenker.ariba.constants;

import java.io.IOException;
import java.util.Properties;

import com.dbschenker.ariba.uploader.App;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Constants {
    
    private Constants() {
    }

    private static Properties properties;

    static {
        properties = new Properties();
        try {
            properties.load(App.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            log.error("Error reading application.properties file", e);
        }
    }

    /**
     * list of constants to be read from properties file
     */
    public static final String URL_PARAM_UPLOAD_CHILD_1 = properties.getProperty("URL_PARAM_UPLOAD_CHILD_1");
    public static final String URL_PARAM_DOWNLOAD_CHILD_1 = properties.getProperty("URL_PARAM_DOWNLOAD_CHILD_1");
    public static final String SHAREDSECRET_VALUE = properties.getProperty("SHAREDSECRET_VALUE");
    public static final String EVENT_VALUE_IMPORT = properties.getProperty("EVENT_VALUE_IMPORT");
    public static final String EVENT_VALUE_EXPORT_COMMON_USERS_UPSTREAM = properties
            .getProperty("EVENT_VALUE_EXPORT_COMMON_USERS_UPSTREAM");
    public static final String EVENT_VALUE_EXPORT_PARTITIONED_USER_DOWNSTREAM = properties
            .getProperty("EVENT_VALUE_EXPORT_PARTITIONED_USER_DOWNSTREAM");

    public static final String PASSWORDADAPTER_1 = properties.getProperty("PASSWORDADAPTER_1");
    public static final String PURCHASE_ORGANIZATION = properties.getProperty("PURCHASE_ORGANIZATION");

    public static final String COMMON_USER_ZIP_FILENAME = properties.getProperty("COMMON_USER_ZIP_FILENAME");
    public static final String PARTITIONED_USER_ZIP_FILENAME = properties.getProperty("PARTITIONED_USER_ZIP_FILENAME");
    public static final String URL_PROXY = properties.getProperty("URL_PROXY");
    public static final String PORT_PROXY = properties.getProperty("PORT_PROXY");
    public static final boolean USE_PROXY = Boolean.parseBoolean(properties.getProperty("USE_PROXY"));
    
    
    public static final String TEMP_FOLDER = properties.getProperty("TEMP_FOLDER");
    /**
     * list of constants needed to communicate with the Ariba application. Will
     * not changed
     */
    public static final String CONTENT_PARAM = "content";
    public static final String EVENT_PARAM = "event";
    public static final String FULLLOAD_PARAM = "fullload";
    public static final String SHAREDSECRET_PARAM = "sharedsecret";
    public static final String CLIENTTYPE_PARAM = "clienttype";
    public static final String CLIENTINFO_PARAM = "clientinfo";
    public static final String CLIENTVERSION_PARAM = "clientversion";
    public static final String CLIENTTYPE_VALUE = "customclient";
    public static final String CLIENTINFO_VALUE = "customclient-info";
    public static final String CLIENTVERSION_VALUE = "1.0";

}
