package com.dbschenker.ariba.uploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.dbschenker.ariba.constants.Constants;
import com.dbschenker.ariba.nonpersistens.entity.CommonUser;
import com.dbschenker.ariba.nonpersistens.entity.NewPartitionedUser;
import com.dbschenker.ariba.nonpersistens.entity.PartitionedUser;
import com.dbschenker.ariba.tools.CSVLoader;

public class AppTest {
    
    @Test
    public void testMain() throws FileNotFoundException, IOException {
        CommonUser cU = new CommonUser();
        cU.setDefaultCurrency("");
        cU.setEmailAddress("testMail@gmail.com");
        cU.setFullName("test user");
        cU.setLoginId("testUser");
        cU.setPasswordAdapter("PasswordAdapter1");
        
        App testApp = new App(false);

        CSVLoader<CommonUser> commonUserLoader = new CSVLoader<CommonUser>(new FileInputStream(testApp.getCommonUsers()),
                CommonUser.class);
        CSVLoader<PartitionedUser> partitionedUserLoader = new CSVLoader<PartitionedUser>(
                new FileInputStream(testApp.getPartitionedUsers()), PartitionedUser.class);

        List<NewPartitionedUser> newPartitionedUsers = new ArrayList<NewPartitionedUser>();

        List<CommonUser> commonUsers = commonUserLoader.load();
        List<PartitionedUser> partitionedUsers = partitionedUserLoader.load();
        for (CommonUser commUser : commonUsers) {
            if (!App.existsInDownstream(commUser, partitionedUsers)) {
                break;
            }
        }

        newPartitionedUsers.add(new NewPartitionedUser(cU));
        File zipFileWithNewUsers = App.writeUsersToFile(newPartitionedUsers);
        testApp.uploadToUrl(Constants.URL_PARAM_UPLOAD_CHILD_1, zipFileWithNewUsers);

    }

    @Test
    public void testMainWithProxy() throws FileNotFoundException, IOException {
        App testApp = new App(true);
        try{
            testApp.runProgram();
        }catch(Exception e){
            
        }        
    }

}
